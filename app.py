from fastapi import FastAPI, HTTPException
from fastapi.responses import FileResponse
from fastapi.staticfiles import StaticFiles
from pydantic import BaseModel
from typing import List
from fastapi.middleware.cors import CORSMiddleware
import psycopg2
import os

app = FastAPI()

static_path = os.path.join(os.path.dirname(__file__), "static")

app.mount("/static", StaticFiles(directory=static_path), name="static")

# Разрешаем CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)

# Подключение к базе данных PostgreSQL
conn = psycopg2.connect(
    host="localhost",
    port=5432,
    database="web",
    user="postgres",
    password="34narolo"
)
cursor = conn.cursor()

# Создание таблицы рецептов, если она не существует
cursor.execute("""
    CREATE TABLE IF NOT EXISTS recipes2 (
        id SERIAL PRIMARY KEY,
        title VARCHAR(100) NOT NULL,
        ingredients TEXT NOT NULL,
        description TEXT NOT NULL
    )
""")
conn.commit()

# Модель данных для запроса добавления рецепта
class RecipeCreate(BaseModel):
    title: str
    ingredients: str
    description: str

# Модель данных для запроса получения списка рецептов
class Recipe(BaseModel):
    id: int
    title: str
    ingredients: str
    description: str

@app.get("/")
async def root() :
    return FileResponse("myenv/index.html")

# Endpoint для добавления нового рецепта
@app.post("/add_recipes", response_model=Recipe)
def add_recipe(recipe: RecipeCreate):
    cursor.execute("""
        INSERT INTO recipes2 (title, ingredients, description)
        VALUES (%s, %s, %s)
        RETURNING id, title, ingredients, description
    """, (recipe.title, recipe.ingredients, recipe.description))
    conn.commit()
    row = cursor.fetchone()
    recipe_data = Recipe(id=row[0], title=row[1], ingredients=row[2], description=row[3])
    return recipe_data

# Endpoint для получения списка рецептов
@app.get("/recipes", response_model=List[Recipe])
def get_recipes():
    cursor.execute("SELECT id, title, ingredients, description FROM recipes2")
    rows = cursor.fetchall()
    recipes = []
    for row in rows:
        recipe = Recipe(id=row[0], title=row[1], ingredients=row[2], description=row[3])
        recipes.append(recipe)
    return recipes

@app.delete("/recipes/{recipe_id}")
def delete_recipe(recipe_id: int):
    cursor.execute("SELECT id FROM recipes2 WHERE id = %s", (recipe_id,))
    row = cursor.fetchone()
    if not row:
        raise HTTPException(status_code=404, detail="Рецепт не найден")
    
    cursor.execute("DELETE FROM recipes2 WHERE id = %s", (recipe_id,))
    conn.commit()
    
    return {"message": f"Рецепт с ID {recipe_id} успешно удален"}