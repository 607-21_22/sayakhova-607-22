PGDMP          !                {            web    12.16    12.16                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                        0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            !           1262    17247    web    DATABASE     �   CREATE DATABASE web WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';
    DROP DATABASE web;
                postgres    false            �            1259    17248    ingredients    TABLE     |   CREATE TABLE public.ingredients (
    id integer NOT NULL,
    name text NOT NULL,
    count integer,
    weight integer
);
    DROP TABLE public.ingredients;
       public         heap    postgres    false            �            1259    17256    recipes    TABLE     �   CREATE TABLE public.recipes (
    id integer NOT NULL,
    ingredients integer,
    description text,
    name text,
    id_rec integer
);
    DROP TABLE public.recipes;
       public         heap    postgres    false            �            1259    17286    recipes2    TABLE     �   CREATE TABLE public.recipes2 (
    id integer NOT NULL,
    title character varying(100) NOT NULL,
    ingredients text NOT NULL,
    description text NOT NULL
);
    DROP TABLE public.recipes2;
       public         heap    postgres    false            �            1259    17284    recipes2_id_seq    SEQUENCE     �   CREATE SEQUENCE public.recipes2_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.recipes2_id_seq;
       public          postgres    false    206            "           0    0    recipes2_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.recipes2_id_seq OWNED BY public.recipes2.id;
          public          postgres    false    205            �            1259    17269    tried_recipes    TABLE     T   CREATE TABLE public.tried_recipes (
    id integer NOT NULL,
    recipes integer
);
 !   DROP TABLE public.tried_recipes;
       public         heap    postgres    false            �
           2604    17289    recipes2 id    DEFAULT     j   ALTER TABLE ONLY public.recipes2 ALTER COLUMN id SET DEFAULT nextval('public.recipes2_id_seq'::regclass);
 :   ALTER TABLE public.recipes2 ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    205    206    206                      0    17248    ingredients 
   TABLE DATA           >   COPY public.ingredients (id, name, count, weight) FROM stdin;
    public          postgres    false    202   ]                 0    17256    recipes 
   TABLE DATA           M   COPY public.recipes (id, ingredients, description, name, id_rec) FROM stdin;
    public          postgres    false    203   �                 0    17286    recipes2 
   TABLE DATA           G   COPY public.recipes2 (id, title, ingredients, description) FROM stdin;
    public          postgres    false    206   7                 0    17269    tried_recipes 
   TABLE DATA           4   COPY public.tried_recipes (id, recipes) FROM stdin;
    public          postgres    false    204   �       #           0    0    recipes2_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.recipes2_id_seq', 16, true);
          public          postgres    false    205            �
           2606    17255    ingredients ingredients_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.ingredients
    ADD CONSTRAINT ingredients_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.ingredients DROP CONSTRAINT ingredients_pkey;
       public            postgres    false    202            �
           2606    17294    recipes2 recipes2_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.recipes2
    ADD CONSTRAINT recipes2_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.recipes2 DROP CONSTRAINT recipes2_pkey;
       public            postgres    false    206            �
           2606    17260    recipes recipes_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.recipes
    ADD CONSTRAINT recipes_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.recipes DROP CONSTRAINT recipes_pkey;
       public            postgres    false    203            �
           2606    17273     tried_recipes tried_recipes_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.tried_recipes
    ADD CONSTRAINT tried_recipes_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.tried_recipes DROP CONSTRAINT tried_recipes_pkey;
       public            postgres    false    204            �
           2606    17261     recipes recipes_ingredients_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.recipes
    ADD CONSTRAINT recipes_ingredients_fkey FOREIGN KEY (ingredients) REFERENCES public.ingredients(id);
 J   ALTER TABLE ONLY public.recipes DROP CONSTRAINT recipes_ingredients_fkey;
       public          postgres    false    202    2704    203            �
           2606    17279 (   tried_recipes tried_recipes_recipes_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.tried_recipes
    ADD CONSTRAINT tried_recipes_recipes_fkey FOREIGN KEY (recipes) REFERENCES public.recipes(id) NOT VALID;
 R   ALTER TABLE ONLY public.tried_recipes DROP CONSTRAINT tried_recipes_recipes_fkey;
       public          postgres    false    203    2706    204               [   x�3�0�¾�v]�wa��N#�?.C��.l��p�	(�ra��@�?Nc.#��j�"@��@���F��vaH��,F��� B,$         _   x�U��	�0�wU���X�5D��
{�'jc��v�Y+�`�ǆ�`G��ȑ��IO���aW��=2��3�`���f��i�jS��D�@�         �  x��XKnG]K��he� ʑ�9�6�!G�'�dPÁ� ��l��8�pH��}��z����F"����^��z����`���-�+�����On즶t?au�xn��-��J{kK�%�te����T���<;?1�?�JiS���n�g��۱1���e�9qs|M��U{'F�[�ng�O�[ؕ{mocc�8�=9,��������D���囻f;ޘ��F�Hm�f�x�}qߦ�0v��!�. �%��7<��n����B�)q�����g��S�SBs-&�Dk��67�d����orOiF�d�.�Y=:;�ӟ�]� �j���{��3{�u[�Abs�#
l+pt��xpd�>hB`qx[%����nR�z��?+��������#��j�,Г/LV&���"�C�
�_��P�zGࢽ>TqŴZ z0II �9��o��!���/"yf��Je��	x�# �Wj`#	����#�*	��x�h�\���C�Шsm���`����8�	2���{+g&>��F���"wm6|��|��g5��6U�+`�2���i(�P�B�h���q͐��	`���d� �Ϟ��\�cf�ө�M���iR9т�ŝ�z �~���:0X�h������0g�L�J[vt�Xw��2ޡ��cx�V�@4]�qBE>Y�7�4�FA�׫ ���vru��|��kt}Z�8,WU�%)�T���]�w��E�<�2���[�=U+(�:��-����&�+��R o_�ԏ%�͐�*����Z޽��ʩmc@� M�<����5P�Rx�HC�㝈��a�0h�G1l�@`0%Vk�A����(n�&��z�J���		�s���� �y
ϥHq��Equ�I����ٞ�S��^bW���r��$ A}a��<K�f� W�+�{y�`�@{��fy\�����
���4�V���`h�{�����x�㋗��,�V���G�V�3���N��|��@x�ds�֜��%P��\����Y+EU�'�/���:��
�?@��h��!�.d��od�k*-aA���	��uN-�������r���s�ū�� ��%�Z7nzd�G��]���5�M֠������:�P�3�����ߎͭto8x|2�+�%�=� 2iM�d]�f��\�M��/y��8�C��ZK�޲���O$M?��XjR�Y�:�%峚ʎb��^���O�(���Γ�Ó�6�l�<�Ƣ{�#�����)	�
�i��tc���]b�8�mb0���al�VP�Z�ob�S�k(2�m�k�G@�����K�R�
g�}EH������H9��7)O/���5�L�Y_�}���y�xo��}�N�I_�hh%�
�:MRw%S�d	%#1l)x�/�h�M��VeEW%��!�y=��nK�?���Ǉ���`��h�'X��(3�"�k֔2T�8c�DM��
U��wx��·���d`� �H��Яk�;TV"��1�E����fL�:	Jd��Zftx�a/��lH��N���/0^9�5P�@
�\��'����R`�C��\�n�-o��s����^��oX,��xHr|�����i���Z��kb�r}k��ߞnH�%�˭>���P����&C�7~xig�A�حB���Ph�@|Wi�����ώ������X�            x�3�4������ M     