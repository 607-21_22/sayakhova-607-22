// Загружаем рецепты при загрузке страницы
window.onload = function() {
    fetchRecipes();
  };
  
  // Функция для отправки POST-запроса на сервер для добавления нового рецепта
  function addRecipe() {
    const title = document.getElementById('recipe-title').value;
    const ingredients = document.getElementById('recipe-ingredients').value;
    const description = document.getElementById('recipe-description').value;
  
    const recipe = {
      title,
      ingredients,
      description
    };
  
    fetch('/add_recipes', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(recipe)
    })
    .then(response => response.json())
    .then(() => {
      // Очищаем поля формы и обновляем список рецептов
      document.getElementById('recipe-title').value = '';
      document.getElementById('recipe-ingredients').value = '';
      document.getElementById('recipe-description').value = '';
  
      fetchRecipes();
    })
    .catch(error => console.error('Ошибка:', error));
  }
  
  // Функция для получения списка рецептов с сервера
  function fetchRecipes() {
    fetch('/recipes')
      .then(response => response.json())
      .then(recipes => {
        const recipesList = document.getElementById('recipes');
        recipesList.innerHTML = '';
  
        recipes.forEach(recipe => {
          const li = document.createElement('li');
          li.innerHTML = `<h4>${recipe.title}</h4>
                          <p><strong>Ингредиенты:</strong> ${recipe.ingredients}</p>
                          <p><strong>Описание:</strong> ${recipe.description}</p>`;
          const deleteButton = document.createElement('button');
          deleteButton.id = 'button_delete';
          deleteButton.textContent = 'Удалить';
          deleteButton.addEventListener('click', () => deleteRecipe(recipe.id));
          li.appendChild(deleteButton);
          
          recipesList.appendChild(li);
        });
      })
      .catch(error => console.error('Ошибка:', error));
  }

  function deleteRecipe(recipeId) {
    fetch(`/recipes/${recipeId}`, { method: 'DELETE' })
      .then(response => response.json())
      .then(() => {
        fetchRecipes();
      })
      .catch(error => console.error('Ошибка:', error));
  }